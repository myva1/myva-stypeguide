import "./set-public-path";
import "./reset.css?modules=false";
import "./global.css?modules=false";

// You can also export React components from this file and import them into your microfrontends
export { default as Button } from "./button.component.js";
